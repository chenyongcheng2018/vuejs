import fetch from 'utils/fetch';

let qs = require('querystring');

export function getStaffInfoList(username, name){
  return fetch({
    url: 'api/sysUsers?username=' + username + '&name=' + name,
    method: 'get'
  })
}

export function saveStaffInfo(form){
  return fetch({
    url: 'api/sysUsers/save',
    method: 'post',
    data:qs.stringify(form)
  })
}

export function getOwStaffRoleList(dbid){
  return fetch({
    url: 'api/sysUsers/' + dbid +'/roles',
    method: 'get'
  })
}

export function getUnStaffRoleList(dbid){
  return fetch({
    url: 'api/sysUsers/' + dbid +'/unroles',
    method: 'get'
  })
}

export function validateUserName(username){
  return fetch({
    url: 'api/sysUsers/validate/username?username=' + username,
    method: 'get'
  })
}

