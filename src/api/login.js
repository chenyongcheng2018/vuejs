import fetch from 'utils/fetch';

let qs = require('querystring');

export function loginByUsername(username, password){
    return fetch({
        url: 'api/login',
        method: 'post',
        data:qs.stringify({username:username, password:password})
    })
}

export function getInfo(username){
    return fetch({
        url: 'api/sysUsers/' + username+'/userinfo',
        method: 'get'
    })
}

export function getMenus(){
    return fetch({
        url: 'api/module',
        method: 'get'
    })
}

export function getAllMenus() {
    return fetch({
      url: 'api/test/allmenu',
      method: 'get'
    });
  }
