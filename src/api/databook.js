import fetch from 'utils/fetch';

let qs = require('querystring');

export function getDatabook(parentid) {
  return fetch({
    url: 'api/databooks',
    method: 'get',
    params:{parentId: parentid}
  })
}

export function loadDatabook(dbid) {
  return fetch({
    url: 'api/databooks/' + dbid,
    method: 'get'
  })
}

export function saveOrUpdate(form) {
  return fetch({
    url: 'api/databooks',
    method: 'post',
    data: qs.stringify(form)
  })
}

export function deleteDatabook(id) {
  return fetch({
    url: 'api/databooks/' + id,
    method: 'delete'
  })
}
