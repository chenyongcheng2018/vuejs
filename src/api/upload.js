import fetch from 'utils/fetch';

let qs = require('querystring');

export function listData(page) {
  return fetch({
    url: 'api/mk-upload/contracts',
    params:page,
    method: 'get',
  })
}

export function loadData(dbid) {
  return fetch({
    url: 'api/mk-upload/contracts/' + dbid,
    method: 'get'
  })
}

export function saveOrUpdate(form) {
  return fetch({
    url: 'api/mk-upload/contracts',
    method: 'post',
    data: qs.stringify(form)
  })
}

export function deleteData(id) {
  return fetch({
    url: 'api/mk-upload/contracts/' + id,
    method: 'delete'
  })


}


export function uploadFile(param){
  return fetch({
    url:'api/mk-upload/contracts/upload',
    method:'post',
    data:param,
    selfDefinedContentType: 'multipart/form-data'
  })
}
