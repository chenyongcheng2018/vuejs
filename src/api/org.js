import fetch from 'utils/fetch';

let qs = require('querystring');

export function getOrgTree(parentid) {
  return fetch({
    url: 'api/orgs/tree',
    method: 'get',
    params:{parentId: parentid}
  })
}

export function getOrg(parentid) {
  return fetch({
    url: 'api/orgs',
    method: 'get',
    params:{parentId: parentid}
  })
}

export function loadOrgs(dbid) {
  return fetch({
    url: 'api/orgs/' + dbid,
    method: 'get'
  })
}

export function saveOrUpdate(form) {
  delete form.children;
  return fetch({
    url: 'api/orgs',
    method: 'post',
    data: qs.stringify(form)
  })
}

export function deleteOrg(id) {
  return fetch({
    url: 'api/hnit-admin/orgs/' + id,
    method: 'delete'
  })
}


export function getOwOrgRoleList(dbid){
  return fetch({
    url: 'api/hnit-admin/orgs/' + dbid +'/roles',
    method: 'get'
  })
}
