import fetch from 'utils/fetch';

let qs = require('querystring');

  export function getObj(id, type) {
    return fetch({
      url: 'api/menu/findByParentId',
      method: 'get',
      params:{parentId:id, type: type}
    });
  }

  export function addObj(obj) {
    return fetch({
      url: 'api/menu/save',
      method: 'post',
      data: qs.stringify(obj)
    });
  }

  export function delObj(id) {
    return fetch({
      url: 'api/menu/delete/' + id,
      method: 'delete'
    })
  }

  export function getExp(id) {
    return fetch({
      url: 'api/menu/findExpression/'+ id,
      method: 'get'
    });
  }

  export function addExp(obj) {
    return fetch({
      url: 'api/menu/saveExpressions',
      method: 'post',
      data: obj,
      selfDefinedContentType: 'application/json'
    });
  }

  export function delExp(id) {
    return fetch({
      url: 'api/menu/deletePermission/' + id,
      method: 'delete'
    })
  }

export function getTreeMenu(id) {
  return fetch({
    url: 'api/menu/treeModules/'+ id,
    method: 'get'
  });
}
