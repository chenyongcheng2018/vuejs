import fetch from 'utils/fetch';

let qs = require('querystring');

export function getRoleInfoList(code, name,start,limit){
  return fetch({
    url: 'api/role/list',
    method: 'get',
    params: {code: code, name: name,start:start,limit:limit}
  })
}

export function getRolePermission(params){//查询该角色已经分配菜单或权限
  return fetch({
    url: 'api/role/rolePermission',
    method: 'get',
    params:params
  })
}
export function saveRoleInfo(form){
  return fetch({
    url: 'api/role/save',
    method: 'post',
    data:qs.stringify(form)
  })
}

export function deleteRoleById(id){
  return fetch({
    url: 'api/role/del/' + id,
    method: 'get'
  })
}

export function getRoleStaffList(roleId){
  return fetch({
    url: 'api/role/roleStaffList/' + roleId,
    method: 'get'
  })
}

export function saveRolePurview(params) {
  return fetch({
    url: 'api/role/saveRolePermission',
    method: 'post',
    data:params,
  selfDefinedContentType: 'application/json'
  })
}

export function getAllRoles(){
  return fetch({
    url: 'api/role/all',
    method: 'get',
    params: {}
  })
}

