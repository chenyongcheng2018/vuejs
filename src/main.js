import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import 'iview/dist/styles/iview.css'
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import NProgress from 'nprogress'; // Progress 进度条
import * as filters from './filters'; // 全局vue filter
import 'vue-multiselect/dist/vue-multiselect.min.css';// 多选框组件css
import Multiselect from 'vue-multiselect';// 使用的一个多选框组件，element-ui的select不能满足所有需求
import Sticky from 'components/Sticky'; // 粘性header组件
import { getToken} from 'utils/auth';
import './icons' // icon

Vue.component('Sticky', Sticky);
Vue.component('multiselect', Multiselect);
Vue.use(Element);

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
  });

const whiteList = ['/login'];// 不重定向白名单
router.beforeEach((to, from, next) => {
    NProgress.start();
    if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
        next()
    } else {
        if(getToken()){
            if (to.path === '/login') {
                next({path: '/'});
            }else{
                if(store.getters.menus === undefined){
                    store.dispatch('GetInfo').then(info =>{
                        store.dispatch("GenerateRouters", info).then(() => {
                            router.addRoutes(store.getters.addRouters)
                            next({ ...to }); // hack方法 确保addRoutes已完成
                        })
                    })
                }else{
                    next();
                }
            }
        }else {
            next('/login'); // 否则全部重定向到登录页
            NProgress.done(); // 在hash模式下 改变手动改变hash 重定向回来 不会触发afterEach 暂时hack方案 ps：history模式下无问题，可删除该行！
        }
    }

})

router.afterEach(() => {

    NProgress.done(); // 结束Progress
});

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
})



