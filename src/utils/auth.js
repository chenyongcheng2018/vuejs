import Cookies from 'js-cookie'

const TokenKey = 'sids'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function getTokenByName(data) {
  return Cookies.get(data)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getCookieByName(name) {
  return Cookies.get(name)
}

export function setCookieByName(name, data) {
  return Cookies.set(name, data)
}

export function removeCookieByName(name) {
  return Cookies.remove(name)
}
