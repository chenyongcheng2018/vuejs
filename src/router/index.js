import Vue from 'vue';
import Router from 'vue-router';
const _import = require('./_import_' + process.env.NODE_ENV);

Vue.use(Router);

import Layout from '../views/layout/Layout'

export const constantRouterMap = [
      {path: '/login',component: _import('login/index'),hidden: true},
      {path: '/404',component: _import('error/404'),hidden: true},
      {path: '/401',component: _import('error/401'),hidden: true},
      {
        path: '',
        component: Layout,
        redirect: 'dashboard',
        children: [{
            path: 'dashboard',
            name: '首页',
            component: _import('dashboard/index'),
            icon: 'dashboard'
        }]
      },
      // {
      //   path: '/introduction',
      //   component: Layout,
      //   redirect: '/introduction/index',
      //   children: [{
      //     path: 'hnit-admin/sysMenu',
      //     component: _import('admin/menu/index'),
      //     name: '简述',
      //     icon: 'documentation'
      //   }]
      // }
]

export default new Router({
    // mode: 'history', //后端支持可开
    scrollBehavior: () => ({
      y: 0
    }),
    routes: constantRouterMap
  });