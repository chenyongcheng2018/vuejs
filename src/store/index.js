// 引入vue
import Vue from 'vue';
// 引入vuex
import Vuex from 'vuex';

import getters from './getters';
import app from './modules/app';
import user from './modules/user';
import permission from './modules/permission';
import databook from './modules/databook';
import org from './modules/org';

// vue根注入vuex的store
Vue.use(Vuex);
// 组装store
const store = new Vuex.Store({
  modules: {
    app,
    permission,
    user,
    databook,
    org
  },getters
 // strict: debug    // 非生产环境使用vue的strict模式，以保证store中的state都是由mutations进行修改
});

export default store
