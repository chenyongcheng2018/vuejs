import {getDatabook, loadDatabook, saveOrUpdate, deleteDatabook} from 'api/databook';
const databook = {
  state: {
    dataBookList: [],
    dataBookEdit: []
  },
  mutations: {
    SetDataBookList: (state, data) => {
      state.dataBookList = data
    },
    SetDataBookEdit: (state, data) => {
      state.dataBookEdit = data
    }
  },
  actions: {
    databookList({commit}, params){
      commit('SetDataBookList', []);
      return new Promise((resolve, reject) => {
        getDatabook(params).then(res => {
          commit('SetDataBookList', res.data);
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    },
    SaveDataBook({commit}, form) {
      return new Promise((resolve, reject) => {
        saveOrUpdate(form).then(res => {
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    },
    loadData({commit}, id) {
      commit('SetDataBookEdit', [])
      return new Promise((resolve, reject) => {
        loadDatabook(id).then(res => {
          commit("SetDataBookEdit", res.data)
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    },
    deleteDataBook({commit}, id) {
      return new Promise((resolve, reject) => {
        deleteDatabook(id).then(res => {
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    }

  }
};
export default databook;
