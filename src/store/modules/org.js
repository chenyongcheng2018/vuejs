import {getOrg, loadOrgs, saveOrUpdate, deleteOrg} from 'api/org';
const org = {
  state: {
    orgList: [],
    orgEdit: []
  },
  mutations: {
    SetOrgList: (state, data) => {
      state.orgList = data
    },
    SetOrgEdit: (state, data) => {
      state.orgEdit = data
    }
  },
  actions: {
    orgList({commit}, params){
      commit('SetOrgList', []);
      return new Promise((resolve, reject) => {
        getOrg(params).then(res => {
          commit('SetOrgList', res.data);
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    },
    SaveOrg({commit}, form) {
      return new Promise((resolve, reject) => {
        saveOrUpdate(form).then(res => {
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    },
    loadData({commit}, id) {
      commit('SetOrgEdit', [])
      return new Promise((resolve, reject) => {
        loadOrgs(id).then(res => {
          commit("SetOrgEdit", res.data)
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    },
    deleteOrg({commit}, id) {
      return new Promise((resolve, reject) => {
        deleteOrg(id).then(res => {
          resolve(res.data);
        }).catch(error => {
          reject(error)
        });
      });
    }

  }
};
export default org;
