import { loginByUsername, getInfo, getMenus} from 'api/login';
import { getToken, setToken, removeToken, getCookieByName, setCookieByName, removeCookieByName} from 'utils/auth';

const user = {
    state : {
        user: '',
        status: '',
        code: '',
        token: getToken(),
        name: '',
        avatar: '',
        introduction: '',
        roles: [],
        menus: undefined,   //用来判断是否需要加载菜单
        elements: undefined,
        permissionMenus: undefined,
        setting: {
            articlePlatform: []
        }

    },

    mutations : {
        SET_CODE: (state, code) => {
            state.code = code;
        },
        SET_TOKEN: (state, token) => {
            state.token = token;
        },
        SET_INTRODUCTION: (state, introduction) => {
            state.introduction = introduction;
        },
        SET_SETTING: (state, setting) => {
            state.setting = setting;
        },
        SET_STATUS: (state, status) => {
            state.status = status;
        },
        SET_NAME: (state, name) => {
            state.name = name;
        },
        SET_AVATAR: (state, avatar) => {
            state.avatar = avatar;
        },
        SET_ROLES: (state, roles) => {
            state.roles = roles;
        },
        SET_MENUS: (state, menus) => {
            state.menus = menus;
        },
        SET_ELEMENTS: (state, elements) => {
            state.elements = elements;
        },
        LOGIN_SUCCESS: () => {
            console.log('login success')
        },
        LOGOUT_USER: state => {
            state.user = '';
        },
        SET_PERMISSION_MENUS: (state, permissionMenus) => {
            state.permissionMenus = permissionMenus;
        }
    },
    actions : {
        LoginByUsername({commit}, param){
            const username = param.username.trim();
            commit('SET_TOKEN', '');
            commit('SET_ROLES', []);
            commit('SET_MENUS', undefined);
            commit('SET_ELEMENTS', undefined);
            removeToken();
            removeCookieByName('username');
            return new Promise((resolve, reject) => {
                loginByUsername(username, param.password).then(res => {
                    const data = res.data;
                    console.log(res)
                    setToken(data.msg);
                    setCookieByName('username', username);
                    commit('SET_NAME', username);
                    commit('SET_TOKEN', data);
                    resolve()
                }).catch(error => {
                    reject(error)
                })
            })
        },
        GetInfo({commit, state}){
            return new Promise((resolve, reject) =>{
                //获取用户基本信息
                getInfo(getCookieByName('username')).then(res =>{
                    console.log("aaaaaaaaaaaaaa")
                    console.log(res)
                    //const data = res.data;
                    commit('SET_ROLES', 'admin');
                    commit('SET_NAME', res.username);
                    commit('SET_AVATAR', '../../../static/image/user.png');
                    commit('SET_INTRODUCTION', res.username);
                    //装载权限
                    const elements = {};
                    for (let i = 0; i < res.permissionInfos.length; i++) {
                        elements[res.permissionInfos[i].code] = true;
                    }
                    commit('SET_ELEMENTS', elements);//将用户权限放置store中
                    resolve(res.menues);
                }).catch( error => {
                  reject(error)
                });
                //获取用户所拥有的菜单，然后与router进行匹配(main.js中守卫方法处理)
                // getMenus(state.token).then(response => {
                //     resolve(response)
                // }).catch( error => {
                //     reject(error)
                // });
            })
        },
        // 前端 登出
        LogOut({
            commit
        }) {
            return new Promise(resolve => {
                console.log('fedlogoutoutouot')
                commit('SET_TOKEN', '');
                commit('SET_MENUS', undefined);
                commit('SET_ELEMENTS', undefined);
                removeToken();
                resolve();
            });
        },
    }
}

export default user;
