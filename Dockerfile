FROM 192.168.1.192:5000/base/nginx:latest
ADD ./nginx.conf /etc/nginx/conf.d/
ADD ./dist.tgz /usr/share/nginx/

EXPOSE 8888
